def lastDigits(N, D):
    p2, p5, p10 = 2 ** D, 5 ** D, 10 ** D
    p, s, n = 1, 0, N
    while n > 0:
        if n > p5:
            s = (s + n // p5) & 1
        for f in range(2, n % p5 + 1):
            if f % 5:
                p = (p * f) % p5
        n //= 5

    i2 = pow(p2, -1, p5)
    n, v = N, 0
    while n:
        n //= 5
        v += n
    i5 = pow(pow(2, v, p5), -1, p5)
    h = p5 - p if s else p
    return (p2 * i2 * h * i5) % p10

print(lastDigits(10 ** 12, 5))