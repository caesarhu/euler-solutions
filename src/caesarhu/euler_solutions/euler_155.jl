function D(n:: Int64) 
    capacitances:: Array{Set{Rational{Int64}}, 1} = fill(Set(), n)
    push!(capacitances[1], 60)   
    for i in 2:n    
        for size_1 in 1:(i ÷ 2)        
            for c1 in capacitances[size_1], c2 in capacitances[i-size_1]             
                push!(capacitances[i], c1 + c2)
                push!(capacitances[i], 1/(1/c1 + 1/c2))           
            end         
        end
    end
    return length(reduce(union, capacitances))
end

@time D(18)
